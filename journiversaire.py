#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
journiversaire is a little application wich help you to calcute
your birthdays in thousands days slices.

Copyright (c) 2007 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import cgi
import datetime
import os.path

print "Content-Type: text/html\n\n"

if os.path.exists("footer.html"):
    f = open("footer.html","r")
    footer = f.read()
    f.close()
else:
    footer = ""

head_css = """<style type="text/css">
.form_ input{
    border:1px solid lime;
    background-color: black;
    color: lime;
    }
.form_input input[type=text], input[type=password] {
    width: 200px;
    padding-left: 2px;
    position: absolute;
    left: 150px;
    }
.form_input input[type=submit] {
    width: 100px;
    position: absolute;
    left: 250px;
    margin-top: 10px;
    }
    </style>"""

def generate_form():
    print """<html>
<head>
    <title>journiversaire</title>
    %s
</head>
<body>
<div class="container">
    <div class="form_input">
    <form method="post" action="journiversaire.py" name="form_input">
    <p>Please, insert your birthday</p>
    <p>Day<input type="text" name="day" value="1"></p>
    <p>Month<input type="text" name="month" value="1"></p>
    <p>Year<input type="text" name="year" value="1970"></p>
    <input type="hidden" name="action" value ="display">
    <input type="submit" value="Enter">
    </form>
    <script type="text/javascript">document.form_input.day.focus();</script>
    </div>
</div>
%s
</body>
</html>""" % (head_css, footer)

def display_journiversaire(day, month, year):
    print """<html>
<head>
    <title>journiversaire</title>
    %s
</head>
<body>
<div class="container">""" % (head_css)
    print "<h1>today : %i days</h1>" % (datetime.datetime.today() - datetime.datetime(year, month, day)).days

    print '<table>'
    print "<tr><th>Thousands days</th><th>Binary days</th></tr>"

    # thousand
    print "<tr>"
    print '<td valign="top">'
    for i in range(1,46):
        date = datetime.datetime(year,month,day)+datetime.timedelta(days=i*1000)
        if date < datetime.datetime.today():
            print '<small><strike>'
        print "%i/%02i/%02i" % (date.year, date.month, date.day)
        print "%d000 days" % i
        print " (%i years)" % int((i*1000)/365.5)
        if date < datetime.datetime.today() :
            print '</strike></small>'
        print "</br>"

    print "</td>"

    # binary
    print '<td valign="top">'
    for i in range(17):
        date = datetime.datetime(year,month,day)+datetime.timedelta(days=2**i)
        if date < datetime.datetime.today():
            print '<small><strike>'
        print "%i/%02i/%02i" % (date.year, date.month, date.day)
        d = 2**i
        if d>1:
            print "%d days" % d
        else:
            print "%d day" % d
        y = int((2**i)/365.5)
        if y > 1:
            print " (%i years)" % y
        else:
            print " (%i year)" % y
        if date < datetime.datetime.today():
            print '</strike></small>'
        print "</br>"
    print "</td>"

    print "</tr>"

    print "</table>"

    print """</div>
%s
</body>
</html>""" % (footer)

def main():
    form = cgi.FieldStorage()
    if (form.has_key("action") and form.has_key("day") and form.has_key("month") and form.has_key("year")):
        if (form["action"].value == "display"):
            day = int(form["day"].value)
            month = int(form["month"].value)
            year = int(form["year"].value)
            display_journiversaire(day, month, year)
    else:
        generate_form()

if __name__ == '__main__':
    main()
